package com.at.library;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.client.RestTemplate;

@Configuration
public class TemplateConfig {

	@Bean
	public RestTemplate resttemplate(){
		return new RestTemplate();
	}
	
}
