package com.at.library.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.at.library.dto.BookDTO;
import com.at.library.dto.BookPostDTO;
import com.at.library.dto.HistoryRentedDTO;
import com.at.library.service.book.BookService;
import com.at.library.service.rent.RentService;


@RestController
@RequestMapping(value = "/book")
public class BookController extends Controller<BookDTO, Integer>{

	@Autowired
	private BookService bookservice;
	
	@Autowired
	private RentService rentservice;
	
	private static final Logger log = 
			LoggerFactory.getLogger(BookController.class);
	
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public List<BookDTO> search(
			@RequestParam(value = "author", required = false) String author,
			@RequestParam(value = "title", required = false) String title,
			@RequestParam(value = "isbn", required = false) String isbn,
			@RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
			@RequestParam(value = "page", required = false, defaultValue = "0") Integer page) throws Exception{
		log.debug("Buscando libros...");
		return bookservice.search(author, title, isbn, size, page);
	}
	
	@RequestMapping(method = RequestMethod.POST)
	public BookDTO create(@RequestBody BookPostDTO book){
		log.debug("Creando libro...");
		return bookservice.create(book);
	}	
	
	@RequestMapping(value = "/{id}/rent", method = RequestMethod.GET)
	public List<HistoryRentedDTO> bookHistory(@PathVariable("id") Integer id,
			@RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
			@RequestParam(value = "page", required = false, defaultValue = "0") Integer page) throws Exception{
		log.debug("Buscando historial de alquileres...");
		return rentservice.rentHistory(id, size, page);
	}
	
}
