package com.at.library.controller;

import java.io.Serializable;
import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.at.library.service.BaseService;

public abstract class Controller <T1, T2 extends Serializable> {

	@Autowired
	BaseService<T1, T2> baseservice;
	
	private static final Logger log = 
			LoggerFactory.getLogger(Controller.class);

	@RequestMapping(method = { RequestMethod.GET })
	public List<T1> getAll(
			@RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
			@RequestParam(value = "page", required = false, defaultValue = "0") Integer page) throws Exception {
		log.debug(String.format("Buscando todos los elementos"));
		return baseservice.findAll(size, page);
	}
	
	@RequestMapping(value = "/{id}", method = {RequestMethod.GET })
	public T1 findOne(@PathVariable("id") T2 id) throws Exception{
		log.debug(String.format("Buscando elemento con id: %s", id));
		return baseservice.findById(id);
	}
	
	@RequestMapping(value = "/{id}", method = { RequestMethod.PUT })
	public void update(@PathVariable("id") T2 id, @RequestBody T1 element){
		log.debug(String.format("Vamos a actualizar el elemento siguiente: %s", element));
		baseservice.update(element);
	}
	
	@RequestMapping(value = "/{id}", method = { RequestMethod.DELETE })
	public void delete(@PathVariable("id") T2 id) throws Exception{
		log.debug(String.format("Vamos a eliminar el elemento con el id siguiente: %s", id));
		baseservice.delete(id);
	}
	
}
