package com.at.library.controller;

import javax.servlet.http.HttpServletRequest;

import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import com.at.library.dto.ExceptionDTO;
import com.at.library.exceptions.BookDoesNotExistsException;
import com.at.library.exceptions.BookNotRentedException;
import com.at.library.exceptions.BookRentedException;
import com.at.library.exceptions.UserDoesNotExistsException;
import com.at.library.exceptions.UserPenalizedException;
import com.at.library.exceptions.SizeNotAllowed;
import com.at.library.exceptions.PageNotAllowed;
import com.at.library.exceptions.RentDoesNotExists;

@ControllerAdvice
public class ExceptionHandlingController {

	@ResponseStatus(value=HttpStatus.BAD_REQUEST)
	@ExceptionHandler(UserPenalizedException.class)
	public @ResponseBody ExceptionDTO penalized(HttpServletRequest request, Exception ex){
		return new ExceptionDTO(request.getRequestURL().toString(), ex.getMessage());
	}
	
	@ResponseStatus(value=HttpStatus.BAD_REQUEST)
	@ExceptionHandler(BookDoesNotExistsException.class)
	public @ResponseBody ExceptionDTO bookNotExists(HttpServletRequest request, Exception ex){
		return new ExceptionDTO(request.getRequestURL().toString(), ex.getMessage());
	}
	
	@ResponseStatus(value=HttpStatus.BAD_REQUEST)
	@ExceptionHandler(BookNotRentedException.class)
	public @ResponseBody ExceptionDTO bookNotRented(HttpServletRequest request, Exception ex){
		return new ExceptionDTO(request.getRequestURL().toString(), ex.getMessage());
	}
	
	@ResponseStatus(value=HttpStatus.BAD_REQUEST)
	@ExceptionHandler(BookRentedException.class)
	public @ResponseBody ExceptionDTO bookRented(HttpServletRequest request, Exception ex){
		return new ExceptionDTO(request.getRequestURL().toString(), ex.getMessage());
	}
	
	@ResponseStatus(value=HttpStatus.BAD_REQUEST)
	@ExceptionHandler(UserDoesNotExistsException.class)
	public @ResponseBody ExceptionDTO userNotExists(HttpServletRequest request, Exception ex){
		return new ExceptionDTO(request.getRequestURL().toString(), ex.getMessage());
	}
	
	@ResponseStatus(value=HttpStatus.BAD_REQUEST)
	@ExceptionHandler(SizeNotAllowed.class)
	public @ResponseBody ExceptionDTO SizeNotAllowed(HttpServletRequest request, Exception ex){
		return new ExceptionDTO(request.getRequestURL().toString(), ex.getMessage());
	}
	
	@ResponseStatus(value=HttpStatus.BAD_REQUEST)
	@ExceptionHandler(PageNotAllowed.class)
	public @ResponseBody ExceptionDTO PageNotAllowed(HttpServletRequest request, Exception ex){
		return new ExceptionDTO(request.getRequestURL().toString(), ex.getMessage());
	}
	
	@ResponseStatus(value=HttpStatus.BAD_REQUEST)
	@ExceptionHandler(RentDoesNotExists.class)
	public @ResponseBody ExceptionDTO RentDoesNotExists(HttpServletRequest request, Exception ex){
		return new ExceptionDTO(request.getRequestURL().toString(), ex.getMessage());
	}
	
}
