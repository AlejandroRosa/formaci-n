package com.at.library.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.at.library.dto.HistoryRentedDTO;
import com.at.library.dto.RentPostDTO;
import com.at.library.service.rent.RentService;

@RestController
@RequestMapping(value = "/rent")
public class RentController{

	@Autowired
	private RentService rentservice;
	
	private static final Logger log = 
			LoggerFactory.getLogger(RentController.class);
	
	@RequestMapping(method = RequestMethod.POST) 
	public HistoryRentedDTO rent(@RequestBody RentPostDTO rent) throws Exception{
		log.debug("Creando alquiler...");
		return rentservice.create(rent);
	}
	
	@RequestMapping(method = RequestMethod.GET)
	public List<HistoryRentedDTO> getAll(
			@RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
			@RequestParam(value = "page", required = false, defaultValue = "0") Integer page) throws Exception{
		log.debug("Viendo alquileres...");
		return rentservice.findAll(size, page);
	}
	
	@RequestMapping(value = "/{id}", method = RequestMethod.GET)
	public HistoryRentedDTO getOne(@PathVariable("id") Integer id) throws Exception{
		log.debug("Buscando alquiler con id: " + id);
		return rentservice.findById(id);
	}
	
	@RequestMapping(value = "/{idBook}", method = { RequestMethod.DELETE })
	public void delete(@PathVariable("idBook") Integer idBook) throws Exception{
		log.debug("Devolviendo libro con id: " + idBook);
		rentservice.delete(idBook);
	}
	
}
