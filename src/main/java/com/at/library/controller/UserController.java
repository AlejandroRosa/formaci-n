package com.at.library.controller;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.at.library.dto.UserDTO;
import com.at.library.service.user.UserService;

@RestController
@RequestMapping(value = "/user")
public class UserController extends Controller<UserDTO, Integer>{

	@Autowired
	private UserService userservice;
	
	private static final Logger log = 
			LoggerFactory.getLogger(UserController.class);
	
	@RequestMapping(value = "/search", method = RequestMethod.GET)
	public List<UserDTO> search(
			@RequestParam(value = "dni", required = false) String dni,
			@RequestParam(value = "name", required = false) String name,
			@RequestParam(value = "city", required = false) String city,
			@RequestParam(value = "size", required = false, defaultValue = "10") Integer size,
			@RequestParam(value = "page", required = false, defaultValue = "0") Integer page) throws Exception{
		log.debug("Buscando usuario...");
		return userservice.search(dni, name, city, size, page);
	}
	
	
	@RequestMapping(method = { RequestMethod.POST })
	public UserDTO create(@RequestBody UserDTO user){
		log.debug("Creando usuario...");
		return userservice.create(user);
	}
	
}
