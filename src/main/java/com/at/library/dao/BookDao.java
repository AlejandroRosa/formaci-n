package com.at.library.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.at.library.model.Book;

@Repository
public interface BookDao extends CrudRepository<Book, Integer> {

	@Query(value = "select b from Book b "
			+ "where (b.author like :author or :author is null)"
			+ " and (b.title like :title or :title is null)"
			+ "and (b.isbn like :isbn or :isbn is null)"
			+ "and b.internalStatus = 'ACTIVE'")
	public List<Book> search(@Param("author") String author, 
			@Param("title") String title,@Param("isbn") String isbn, Pageable pageable); 
	
	@Query(value = "select b from Book b where b.internalStatus = 'ACTIVE'")
	Page<Book> findAll(Pageable pageable);

}
