package com.at.library.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;

import com.at.library.model.Rent;

public interface RentDAO extends CrudRepository<Rent, Integer> {
	
	@Query(value = "select r from Rent r where r.book.id = ?1 "
			+ "and r.book.internalStatus='ACTIVE' and r.user.status='ACTIVE'")
	public List<Rent> rentHistory(Integer id_book, Pageable pageable);
	
	@Query(value = "select r from Rent r where r.book.id = ?1 and r.end is null")
	public Rent isRented(Integer id_book);

	@Query(value = "select r from Rent r where r.book.internalStatus='ACTIVE' and r.user.status='ACTIVE'")
	Page<Rent> findAll(Pageable pageable);
	
}
