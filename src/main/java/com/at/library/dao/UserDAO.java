package com.at.library.dao;

import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import com.at.library.model.Rent;
import com.at.library.model.User;

@Repository
public interface UserDAO extends CrudRepository<User, Integer> {
	
	@Query(value = "select r from Rent r where r.user.id = ?1")
	public List<Rent> userRents(Integer id_user);

	@Query(value = "select u from User u "
			+ "where (u.dni like :dni or :dni is null)"
			+ " and (u.name like :name or :name is null)"
			+ "and (u.city like :city or :city is null)"
			+ "and u.status='ACTIVE'")
	public List<User> search(@Param("dni") String dni, 
			@Param("name") String name, @Param("city") String city, Pageable pageable);
	
	@Query(value = "select u from User u where u.status='ACTIVE'")
	Page<User> findAll(Pageable pageable);
	
}
