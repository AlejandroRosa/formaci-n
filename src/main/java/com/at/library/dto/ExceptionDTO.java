package com.at.library.dto;

import java.io.Serializable;

public class ExceptionDTO implements Serializable {

	private static final long serialVersionUID = 4877899717773766021L;

	private String url;
	
	private String message;
	
	public ExceptionDTO(String url, String message){
		
		this.url = url;
		this.message = message;
		
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getMessage() {
		return message;
	}

	public void setMessage(String message) {
		this.message = message;
	}

}
