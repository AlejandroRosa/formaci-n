package com.at.library.dto;

import java.io.Serializable;
import java.util.Date;

import com.fasterxml.jackson.annotation.JsonFormat;

public class HistoryRentedDTO implements Serializable {
	
	private static final long serialVersionUID = -3901386945500422804L;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date init;
	
	@JsonFormat(pattern="yyyy-MM-dd")
	private Date end;
	
	private String title;
	
	private Integer idBook;
	
	private String dni;
	
	private Integer idUser;

	public Date getInit() {
		return init;
	}

	public void setInit(Date init) {
		this.init = init;
	}

	public Date getEnd() {
		return end;
	}

	public void setEnd(Date end) {
		this.end = end;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Integer getIdBook() {
		return idBook;
	}

	public void setIdBook(Integer idBook) {
		this.idBook = idBook;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public Integer getIdUser() {
		return idUser;
	}

	public void setIdUser(Integer idUser) {
		this.idUser = idUser;
	}
	
}
