package com.at.library.dto;

import java.io.Serializable;

public class ImagesDTO implements Serializable{

	private static final long serialVersionUID = -7275584554407378168L;

	private String smallThumbnail;
	
	private String thumbnail;

	public String getSmallThumbnail() {
		return smallThumbnail;
	}

	public void setSmallThumbnail(String smallThumbnail) {
		this.smallThumbnail = smallThumbnail;
	}

	public String getThumbnail() {
		return thumbnail;
	}

	public void setThumbnail(String thumbnail) {
		this.thumbnail = thumbnail;
	}
	
}
