package com.at.library.dto;

import java.io.Serializable;

public class ItemsDTO implements Serializable{

	private static final long serialVersionUID = -4531207756603986638L;

	private VolumeInfoDTO volumeInfo;

	private String id;

	public String getId() {
		return id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public VolumeInfoDTO getVolumeInfo() {
		return volumeInfo;
	}

	public void setVolumeInfo(VolumeInfoDTO volumeInfo) {
		this.volumeInfo = volumeInfo;
	}
	
}
