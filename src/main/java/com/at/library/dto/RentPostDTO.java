package com.at.library.dto;

import java.io.Serializable;

public class RentPostDTO implements Serializable{

	private static final long serialVersionUID = 6309654884780955624L;

	private Integer book;
	
	private Integer user;

	public Integer getBook() {
		return book;
	}

	public void setBook(Integer book) {
		this.book = book;
	}

	public Integer getUser() {
		return user;
	}

	public void setUser(Integer user) {
		this.user = user;
	}
	
}
