package com.at.library.dto;

import java.io.Serializable;
import java.util.Date;

public class UserDTO implements Serializable{

	private static final long serialVersionUID = 3827455324138571715L;
	
	private String name;
	
	private String dni;
	
	private String city;
		
	public Integer id;
	
	public Date datePenalized;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public Date getDatePenalized() {
		return datePenalized;
	}

	public void setDatePenalized(Date datePenalized) {
		this.datePenalized = datePenalized;
	}

}
