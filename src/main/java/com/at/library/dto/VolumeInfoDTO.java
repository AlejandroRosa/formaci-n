package com.at.library.dto;

import java.io.Serializable;

public class VolumeInfoDTO implements Serializable{
	
	private static final long serialVersionUID = -8325528789813358005L;

	private ImagesDTO imageLinks;
	
	private String description;
	
	private String publishedDate;

	public ImagesDTO getImageLinks() {
		return imageLinks;
	}

	public void setImageLinks(ImagesDTO imageLinks) {
		this.imageLinks = imageLinks;
	}

	public String getDescription() {
		return description;
	}

	public void setDescription(String description) {
		this.description = description;
	}

	public String getPublishedDate() {
		return publishedDate;
	}

	public void setPublishedDate(String publishedDate) {
		this.publishedDate = publishedDate;
	}
}
