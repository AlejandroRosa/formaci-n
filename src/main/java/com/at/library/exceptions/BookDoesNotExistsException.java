package com.at.library.exceptions;

public class BookDoesNotExistsException extends Exception {

	private static final long serialVersionUID = -1286106756427547257L;

	public BookDoesNotExistsException(){
		super("El libro no existe");
	}
	
}
