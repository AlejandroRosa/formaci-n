package com.at.library.exceptions;

public class BookNotRentedException extends Exception {

	private static final long serialVersionUID = 2876940792567199389L;

	public BookNotRentedException(){
		super("El libro con el id especificado no está alquilado");
	}
	
}
