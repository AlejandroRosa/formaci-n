package com.at.library.exceptions;

public class BookRentedException extends Exception {

	private static final long serialVersionUID = -505077617238898084L;

	public BookRentedException(){
		super("El libro está alquilado");
	}
	
}
