package com.at.library.exceptions;

public class PageNotAllowed extends Exception {

	private static final long serialVersionUID = 2324924530354269043L;

	public PageNotAllowed(){
		super("La página debe ser no negativa");
	}
	
}
