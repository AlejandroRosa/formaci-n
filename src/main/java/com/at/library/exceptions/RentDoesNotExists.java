package com.at.library.exceptions;

public class RentDoesNotExists extends Exception {

	private static final long serialVersionUID = 6947340367272612236L;

	public RentDoesNotExists(){
		super("El alquiler o los alquileres solicitados no existen");
	}
	
}
