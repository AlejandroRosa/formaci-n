package com.at.library.exceptions;

public class RentEndedException extends Exception {

	private static final long serialVersionUID = 2160533676000263187L;

	public RentEndedException(){
		super("El libro ya ha sido devuelto");
	}
	
}
