package com.at.library.exceptions;

public class SizeNotAllowed extends Exception {

	private static final long serialVersionUID = -123109501014556422L;

	public SizeNotAllowed(){
		super("El tamaño debe ser positivo");
	}
	
}
