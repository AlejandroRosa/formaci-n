package com.at.library.exceptions;

public class UserDoesNotExistsException extends Exception {

	private static final long serialVersionUID = 6593607034671310427L;

	public UserDoesNotExistsException(){
		super("El usuario no existe");
	}
	
}
