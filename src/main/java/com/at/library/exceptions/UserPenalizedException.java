package com.at.library.exceptions;

public class UserPenalizedException extends Exception {

	private static final long serialVersionUID = 4036313433204772494L;

	public UserPenalizedException() {
		super("El usuario está penalizado");
	}
	
}
