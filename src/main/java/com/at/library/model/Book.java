package com.at.library.model;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.at.library.enums.StatusBook;
import com.at.library.enums.StatusEnum;

@Entity
public class Book implements Serializable {

	private static final long serialVersionUID = 6374272004167410735L;

	@Id
	@GeneratedValue
	private Integer id;

	private String isbn;

	private String title;

	private String author;
	
	private String image;
	
	private Integer year;
	
	private String description;
	
	@Enumerated(EnumType.STRING)
	private StatusEnum internalStatus = StatusEnum.ACTIVE;
	
	@ManyToOne(fetch = FetchType.LAZY)
	private Section section;

	@Enumerated(EnumType.STRING)
	private StatusBook status;

	@Temporal(TemporalType.DATE)
	private Date startDate;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getIsbn() {
		return isbn;
	}

	public void setIsbn(String isbn) {
		this.isbn = isbn;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public StatusBook getStatus() {
		return status;
	}

	public void setStatus(StatusBook status) {
		this.status = status;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Section getSection() {
		return section;
	}

	public void setSection(Section section) {
		this.section = section;
	}

	public String getImage() {
		return image;
	}

	public void setImage(String image) {
		this.image = image;
	}

	public Integer getYear() {
		return year;
	}

	public void setYear(Integer year) {
		this.year = year;
	}

	public String getDescription() {
		return description;
	}

	public StatusEnum getInternalStatus() {
		return internalStatus;
	}

	public void setInternalStatus(StatusEnum internalStatus) {
		this.internalStatus = internalStatus;
	}

	public void setDescription(String description) {
		this.description = description;
	}

}
