package com.at.library.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;

@Entity
public class Employee implements Serializable{

	private static final long serialVersionUID = -6958494906030230973L;

	@Id
	@GeneratedValue
	private Integer id;
	
	private String name;
	
	private String dni;
	
	@OneToMany(fetch = FetchType.LAZY, mappedBy = "employee")
	private List<Rent> rents = new ArrayList<>();
	
	@OneToMany(fetch = FetchType.LAZY)
	private List<User> users = new ArrayList<>();

	public List<User> getUsers() {
		return users;
	}

	public void setUsers(List<User> users) {
		this.users = users;
	}

	public Integer getId(){
		return id;
	}
	
	public void setId(Integer id){
		this.id = id;
	}

	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getDni(){
		return dni;
	}
	
	public void setDni(String dni){
		this.dni = dni;
	}

	public List<Rent> getRents() {
		return rents;
	}

	public void setRents(List<Rent> rents) {
		this.rents = rents;
	}
	
}
