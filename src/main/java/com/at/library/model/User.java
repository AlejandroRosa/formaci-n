package com.at.library.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

import com.at.library.enums.StatusEnum;

@Entity
public class User implements Serializable{

	private static final long serialVersionUID = -4920042284292856239L;

	@Id
	@GeneratedValue
	private Integer id;
	
	private String name;
	
	private String dni;
	
	private String city;
	
	@Enumerated(EnumType.STRING)
	private StatusEnum status = StatusEnum.ACTIVE;

	@OneToMany(fetch = FetchType.LAZY, mappedBy = "user")
	private List<Rent> rents = new ArrayList<>();
	
	@Temporal(TemporalType.DATE)
	private Date datePenalized;
	
	public Date getDatePenalized() {
		return datePenalized;
	}

	public void setDatePenalized(Date datePenalized) {
		this.datePenalized = datePenalized;
	}

	public Integer getId(){
		return id;
	}
	
	public void setId(Integer id){
		this.id = id;
	}

	public String getName(){
		return name;
	}
	
	public void setName(String name){
		this.name = name;
	}
	
	public String getDni(){
		return dni;
	}
	
	public void setDni(String dni){
		this.dni = dni;
	}
	
	public String getCity(){
		return city;
	}
	
	public void setCity(String city){
		this.city = city;
	}

	public List<Rent> getRents() {
		return rents;
	}

	public void setRents(List<Rent> rents) {
		this.rents = rents;
	}
	
	
	public StatusEnum getStatus() {
		return status;
	}

	public void setStatus(StatusEnum status) {
		this.status = status;
	}
	
}
