package com.at.library.service;

import java.io.Serializable;
import java.util.List;

public interface BaseService<T1, T2 extends Serializable> {
		
	/**
	 * Busca los elementos en la BD
	 * 
	 * @param size, page
	 * @return
	 */
	public List<T1> findAll(Integer size, Integer page) throws Exception;
	
	/**
	 * Crea un elemento en la BD
	 * 
	 * @param element
	 * @return
	 */
	public T1 create( T1 element );
	/**
	 * Actualiza un elemento
	 * 
	 * @param element
	 * @return
	 */
	public void update( T1 element ); 
	/**
	 * Elimina el elemento con el id especificado
	 * 
	 * @param id
	 * @return
	 */
	public void delete( T2 id ) throws Exception;
	
	/**
	 * Busca el elemento con el id
	 * 
	 * @param id
	 * @return
	 */
	public T1 findById( T2 id ) throws Exception;
}