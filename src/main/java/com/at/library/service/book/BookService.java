package com.at.library.service.book;

import java.util.List;

import com.at.library.dto.BookDTO;
import com.at.library.dto.BookPostDTO;
import com.at.library.model.Book;
import com.at.library.service.BaseService;

public interface BookService extends BaseService<BookDTO, Integer>{

	/**
	 * Transforma un libro en un libroDTO
	 * 
	 * @param book
	 * @return
	 */
	BookDTO transform(Book book);

	/**
	 * Transforma un libroDTO en un libro
	 * 
	 * @param book
	 * @return
	 */
	Book transform(BookDTO book);

	/**
	 * Busca un libro con los parámetros
	 * 
	 * @param author, title, isbn, size, page
	 * @return
	 */
	List<BookDTO> search(String author, String title, String isbn, Integer size, Integer page) throws Exception;

	/**
	 * Crea un libro
	 * 
	 * @param book
	 * @return
	 */
	BookDTO create(BookPostDTO book);

}
