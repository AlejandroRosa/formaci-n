package com.at.library.service.book;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.RestTemplate;

import com.at.library.dao.BookDao;
import com.at.library.dto.BookDTO;
import com.at.library.dto.BookGoogleDTO;
import com.at.library.dto.BookPostDTO;
import com.at.library.enums.StatusBook;
import com.at.library.enums.StatusEnum;
import com.at.library.exceptions.BookDoesNotExistsException;
import com.at.library.exceptions.PageNotAllowed;
import com.at.library.exceptions.SizeNotAllowed;
import com.at.library.model.Book;

@Service
public class BookServiceImpl implements BookService {

	@Autowired
	private BookDao bookDao;

	@Autowired
	private DozerBeanMapper dozer;
	
	@Autowired
	private RestTemplate resttemplate;

	@Override
	@Transactional( readOnly = true )
	public List<BookDTO> findAll(Integer size, Integer page) throws Exception {
		
		if(size < 1) throw new SizeNotAllowed();
		if(page < 0) throw new PageNotAllowed();
		
		final Iterable<Book> findAll = bookDao.findAll(new PageRequest(page, size));
		final Iterator<Book> iterator = findAll.iterator();
		final List<BookDTO> res = new ArrayList<>();
		while (iterator.hasNext()) {
			final Book b = iterator.next();
			final BookDTO bDTO = transform(b);
			res.add(bDTO);
		}
		return res;
	}

	@Override
	public BookDTO transform(Book book) {
		return dozer.map(book, BookDTO.class);
	}

	@Override
	public Book transform(BookDTO book) {
		return dozer.map(book, Book.class);
	}

	@Override
	public BookDTO create(BookDTO book) {
		final Book b = transform(book);
		return transform(bookDao.save(b));
	}
	
	public BookDTO transform(BookPostDTO book){
		
		String url = "https://www.googleapis.com/books/v1/volumes?q=intitle:" + book.getTitle() + "&maxResults=1";
		final BookGoogleDTO bGoogle = resttemplate.getForObject(url, BookGoogleDTO.class);
		BookDTO bookDTO = new BookDTO();
		bookDTO.setAuthor(book.getAuthor());
		bookDTO.setIsbn(book.getIsbn());
		bookDTO.setTitle(book.getTitle());
		bookDTO.setStatus(StatusBook.OK.toString());
		if(bGoogle.getItems() != null){
			if(bGoogle.getItems().get(0).getVolumeInfo() != null){
				if(bGoogle.getItems().get(0).getVolumeInfo().getImageLinks() != null)
					bookDTO.setImage(bGoogle.getItems().get(0).getVolumeInfo().getImageLinks().getThumbnail());
				bookDTO.setDescription(bGoogle.getItems().get(0).getVolumeInfo().getDescription());
				bookDTO.setYear(Integer.parseInt(bGoogle.getItems().get(0).getVolumeInfo().getPublishedDate().substring(0,4)));
			}
		}
		return bookDTO;
	}
	
	@Override
	public BookDTO create(BookPostDTO book) {
		BookDTO bookdto = transform(book);
		return transform(bookDao.save(transform(bookdto)));
	}

	@Override
	@Transactional( readOnly = true )
	public BookDTO findById(Integer id) throws BookDoesNotExistsException {
		final Book b = bookDao.findOne(id);
		if(b == null) throw new BookDoesNotExistsException();
		if(b.getInternalStatus() == StatusEnum.DISABLE) throw new BookDoesNotExistsException();
		return transform(b);
	}

	@Override
	public void update(BookDTO book) {
		final Book b = transform(book);
		bookDao.save(b);
	}

	@Override
	public void delete(Integer id) throws BookDoesNotExistsException {
		Book book = bookDao.findOne(id);
		if(book == null) throw new BookDoesNotExistsException();
		if(book.getInternalStatus() == StatusEnum.DISABLE) throw new BookDoesNotExistsException();
		book.setInternalStatus(StatusEnum.DISABLE);
		bookDao.save(book);
	}

	@Override
	@Transactional( readOnly = true )
	public List<BookDTO> search(String author, String title, 
			String isbn, Integer size, Integer page) throws Exception{
		
		if(size < 1) throw new SizeNotAllowed();
		if(page < 0) throw new PageNotAllowed();
		
		if(author != null) author = "%" + author + "%";
		if(title != null) title = "%" + title + "%";
		if(isbn != null) isbn = "%" + isbn + "%";
		
		List<BookDTO> books = new ArrayList<>();
		Iterator<Book> iteratorBook = bookDao.search(author,title,isbn, new PageRequest(page, size)).iterator();
		
		while(iteratorBook.hasNext()){
			books.add(transform(iteratorBook.next()));
		}
			
		return books;

	}

}
