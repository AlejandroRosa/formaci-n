package com.at.library.service.rent;

import java.util.List;

import com.at.library.dto.HistoryRentedDTO;
import com.at.library.dto.RentPostDTO;
import com.at.library.model.Rent;

public interface RentService {

	/**
	 * Transforma un rent en un HistoryRentedDTO
	 * 
	 * @param rent
	 * @return
	 */
	HistoryRentedDTO transform(Rent rent);
	
	/**
	 * Busca el alquiler con el id
	 * 
	 * @param id
	 * @return
	 */
	HistoryRentedDTO findById(Integer id) throws Exception;

	/**
	 * Devuelve el libro con el id
	 * 
	 * @param idBook
	 * @return
	 */
	void delete(Integer idBook) throws Exception;

	/**
	 * Comienza un alquiler
	 * 
	 * @param rent
	 * @return
	 */
	HistoryRentedDTO create(RentPostDTO rent) throws Exception;

	/**
	 * Devuelve los alquileres
	 * 
	 * @param size, page
	 * @return
	 */
	List<HistoryRentedDTO> findAll(Integer size, Integer page) throws Exception;

	/**
	 * Devuelve los alquileres de un libro
	 * 
	 * @param id, size, page
	 * @return
	 */
	List<HistoryRentedDTO> rentHistory(Integer id, Integer size, Integer page) throws Exception;

	/**
	 * Devuelve los alquileres
	 * 
	 * @param size, page
	 * @return
	 */
	List<Rent> getAllRents(Integer size, Integer page);
	
}
