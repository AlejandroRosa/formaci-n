package com.at.library.service.rent;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Iterator;
import java.util.List;

import org.dozer.DozerBeanMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.at.library.dao.RentDAO;
import com.at.library.dto.HistoryRentedDTO;
import com.at.library.dto.RentPostDTO;
import com.at.library.enums.StatusBook;
import com.at.library.enums.StatusEnum;
import com.at.library.exceptions.BookDoesNotExistsException;
import com.at.library.exceptions.BookNotRentedException;
import com.at.library.exceptions.BookRentedException;
import com.at.library.exceptions.PageNotAllowed;
import com.at.library.exceptions.RentDoesNotExists;
import com.at.library.exceptions.RentEndedException;
import com.at.library.exceptions.SizeNotAllowed;
import com.at.library.exceptions.UserDoesNotExistsException;
import com.at.library.exceptions.UserPenalizedException;
import com.at.library.model.Book;
import com.at.library.model.Rent;
import com.at.library.model.User;
import com.at.library.service.book.BookService;
import com.at.library.service.user.UserService;


@Service
public class RentServiceImpl implements RentService{
	 
	@Autowired
	private RentDAO rentdao;
	
	@Autowired
	private UserService userservice;
	
	@Autowired
	private BookService bookservice;
	
	@Autowired
	private DozerBeanMapper dozer;
	
	@Override
	@Transactional( readOnly = true )
	public List<HistoryRentedDTO> findAll(Integer size, Integer page) throws Exception {
		
		if(size < 1) throw new SizeNotAllowed();
		if(page < 0) throw new PageNotAllowed();
		
		Iterator<Rent> rentIterator = rentdao.findAll(new PageRequest(page, size)).iterator();
		List<HistoryRentedDTO> rents = new ArrayList<>();
		
		while(rentIterator.hasNext()){
			Rent r = rentIterator.next();
			rents.add(transform(r));
		}
		
		return rents;
	}

	@Override
	@Transactional( readOnly = true )
	public List<Rent> getAllRents(Integer size, Integer page){
		return rentdao.findAll(new PageRequest(page, size)).getContent();
	}
	
	public HistoryRentedDTO transform(Rent rent){
		return dozer.map(rent, HistoryRentedDTO.class);
	}

	public HistoryRentedDTO transformToHistory(Rent rent){
		return dozer.map(rent, HistoryRentedDTO.class);
	}
	
	@Override
	public HistoryRentedDTO create(RentPostDTO rent) throws Exception {
	
		if(rent.getBook() == null) 
			throw new BookDoesNotExistsException();
		if(rent.getUser() == null) 
			throw new UserDoesNotExistsException(); 
		
		Book b = bookservice.transform(bookservice.findById(rent.getBook()));
		User u = userservice.transform(userservice.findById(rent.getUser()));
		
		if(b.getStatus() == StatusBook.OK){
			if(u.getStatus() == StatusEnum.ACTIVE){
				if(u.getDatePenalized() == null){
					if(b.getInternalStatus() == StatusEnum.ACTIVE){
						Rent r = new Rent();
						r.setBook(b); r.setUser(u); r.setInit(Calendar.getInstance().getTime());
						b.setStatus(StatusBook.RENTED);
						bookservice.update(bookservice.transform(b));
						return transform(rentdao.save(r));
					}
					else throw new BookDoesNotExistsException();
				}
				else throw new UserPenalizedException();
			}
			else throw new UserDoesNotExistsException();
		}
		else throw new BookRentedException();
	}

	@Override
	public void delete(Integer idBook) throws Exception {
		Rent rent = rentdao.isRented(idBook);
		if(rent != null){
			if(rent.getEnd() != null) throw new RentEndedException();
			rent.setEnd(new Date());
			rentdao.save(rent);
			Book book = rent.getBook();
			book.setStatus(StatusBook.OK);
			bookservice.update(bookservice.transform(book));
		}
		else throw new BookNotRentedException();
	}

	@Override
	@Transactional( readOnly = true )
	public HistoryRentedDTO findById(Integer id) throws Exception {
		Rent r = rentdao.findOne(id);
		if(r == null) throw new RentDoesNotExists();
		if(r.getUser().getStatus() == StatusEnum.DISABLE) throw new UserDoesNotExistsException();
		if(r.getBook().getInternalStatus() == StatusEnum.DISABLE) throw new BookDoesNotExistsException();
		return transform(r);
	}
	
	@Override
	@Transactional( readOnly = true )
	public List<HistoryRentedDTO> rentHistory(Integer id, Integer size, Integer page) throws Exception{
		
		if(size < 1) throw new SizeNotAllowed();
		if(page < 0) throw new PageNotAllowed();
		
		List<HistoryRentedDTO> rents = new ArrayList<>();
		Iterator<Rent> iteratorRent = rentdao.rentHistory(id, new PageRequest(page, size)).iterator();
		
		while(iteratorRent.hasNext()){
			rents.add(transformToHistory(iteratorRent.next()));
		}
		if(rents.size() == 0) throw new RentDoesNotExists();
		return rents;
	}
	
}
