package com.at.library.service.user;


import java.util.List;

import com.at.library.dto.UserDTO;
import com.at.library.model.User;
import com.at.library.service.BaseService;

public interface UserService extends BaseService<UserDTO, Integer>{
	
	/**
	 * Transforma un user en un userDTO
	 * 
	 * @param user
	 * @return
	 */
	UserDTO transform(User user);
	
	/**
	 * Transforma un userDTO en un user
	 * 
	 * @param user
	 * @return
	 */
	User transform(UserDTO user);
		
	/**
	 * Penaliza a los usuarios que no han devuelto un libro en la fecha
	 * 
	 * @param 
	 * @return
	 */
	void penalize();
	
	/**
	 * Mira qué usuarios han cumplido su castigo
	 * 
	 * @param 
	 * @return
	 */
	void forgive();

	/**
	 * Busca usuarios
	 * 
	 * @param dni, name, city, size, page
	 * @return
	 */
	List<UserDTO> search(String dni, String name, String city, Integer size, Integer page) throws Exception;

}
