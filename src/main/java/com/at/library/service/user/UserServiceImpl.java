package com.at.library.service.user;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.dozer.DozerBeanMapper;
import org.joda.time.DateTime;
import org.joda.time.Days;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.at.library.dao.UserDAO;
import com.at.library.dto.UserDTO;
import com.at.library.enums.StatusEnum;
import com.at.library.exceptions.PageNotAllowed;
import com.at.library.exceptions.SizeNotAllowed;
import com.at.library.exceptions.UserDoesNotExistsException;
import com.at.library.model.Rent;
import com.at.library.model.User;
import com.at.library.service.rent.RentService;

@Service
public class UserServiceImpl implements UserService{

	@Autowired
	private UserDAO userdao;
	
	@Autowired
	private DozerBeanMapper dozer;
	
	@Autowired
	private RentService rentservice;
		
	@Override
	@Transactional( readOnly = true )
	public List<UserDTO> findAll(Integer size, Integer page) throws Exception {
		
		if(size < 1) throw new SizeNotAllowed();
		if(page < 0) throw new PageNotAllowed();
		
		final Iterator<User> usersIterator = userdao.findAll(new PageRequest(page, size)).iterator();
		final List<UserDTO> users = new ArrayList<>();
		
		while(usersIterator.hasNext()){
			final User u = usersIterator.next();
			users.add(transform(u));
		}
		
		return users;
		
	}

	@Override
	public UserDTO create(UserDTO user){
		User u = transform(user);
		return transform(userdao.save(u));
	}

	public UserDTO transform(User u) {
		return dozer.map(u, UserDTO.class);
	}

	public User transform(UserDTO u) {
		return dozer.map(u, User.class);
	}

	@Override
	public void delete(Integer id) throws UserDoesNotExistsException {
		User u = userdao.findOne(id);
		if(u == null) throw new UserDoesNotExistsException();
		if(u.getStatus() == StatusEnum.DISABLE) throw new UserDoesNotExistsException();
		u.setStatus(StatusEnum.DISABLE);
		userdao.save(u);
	}

	@Override
	public void update(UserDTO element) {
		userdao.save(transform(element));
	}

	@Override
	@Transactional( readOnly = true )
	public UserDTO findById(Integer id) throws UserDoesNotExistsException {
		User user = userdao.findOne(id);
		if(user == null) throw new UserDoesNotExistsException();
		if(user.getStatus() == StatusEnum.DISABLE) throw new UserDoesNotExistsException();
		return transform(user);
	}
	
	@Override
	@Scheduled(cron = "0 0 1 * * ?")
	public void penalize(){
		
		Integer size = 10, page = 0;
		Iterator<Rent> rentIterator = rentservice.getAllRents(size, page).iterator();
		Map<Integer, Date> map = new HashMap<Integer, Date>();
		Calendar c = Calendar.getInstance();

		boolean ended = false;
		while(!ended){
			while(rentIterator.hasNext()){
				Rent rent = rentIterator.next();
				Integer userId = rent.getIdUser();
				c.setTime(rent.getInit());
				c.add(Calendar.DATE, 5);
				if(rent.getEnd() == null){
					if(c.getTime().before(Calendar.getInstance().getTime())){
						if(map.get(userId) == null || map.get(userId).before(rent.getInit())){
							map.put(userId, rent.getInit());
						}
					}
				}
			}
			page++;
			rentIterator = rentservice.getAllRents(size, page).iterator();
			if(!rentIterator.hasNext()) ended = true;
		}
		
		Iterator<Integer> it = map.keySet().iterator();
		
		DateTime end = new DateTime();
		
		while(it.hasNext()){
			Integer key = it.next();
			Date date = map.get(key);
			if(date != null){
				User u = userdao.findOne(key);
				DateTime start = new DateTime(map.get(key));
				Integer days = Days.daysBetween(start, end).getDays();
				days = days * 3;
				DateTime datePenalized = new DateTime();
				datePenalized = datePenalized.plusDays(days);
				if(u.getDatePenalized() == null || datePenalized.toDate().after(u.getDatePenalized())){
					u.setDatePenalized(datePenalized.toDate());
					userdao.save(u);
				}
			}
		}
	}
	
	@Override
	@Scheduled(cron = "0 0 2 * * ?")
	public void forgive(){
		Iterator<User> userIterator = userdao.findAll().iterator();
		while(userIterator.hasNext()){
			User u = userIterator.next();
			if(u.getDatePenalized() != null){
				if(Calendar.getInstance().getTime().after(u.getDatePenalized())){
					u.setDatePenalized(null);
					userdao.save(u);
				}
			}
		}
	}
	
	@Override
	@Transactional( readOnly = true )
	public List<UserDTO> search(String dni, String name, 
			String city, Integer size, Integer page) throws Exception{
		
		if(size < 1) throw new SizeNotAllowed();
		if(page < 0) throw new PageNotAllowed();
		
		if(dni != null) dni = "%" + dni + "%";
		if(name != null) name = "%" + name + "%";
		if(city != null) city = "%" + city + "%";
		
		List<UserDTO> users = new ArrayList<>();
		Iterator<User> iteratorUser = userdao.search(dni,name,city, new PageRequest(page, size)).iterator();
		
		while(iteratorUser.hasNext()){
			users.add(transform(iteratorUser.next()));
		}
			
		return users;
	}
	
}
